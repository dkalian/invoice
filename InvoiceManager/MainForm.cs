﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InvoiceManager
{
    public partial class MainForm : Form
    {
        public const string DATA_TABLE_NAME = "invoice_data_table";
        public const string DATA_SET_NAME = "invoice_data_set";
        private AbstractDataStorage<Invoice> dataStorage;
        private SortableBindingList<Invoice> invoiceSortableBindingList;
        private BindingSource invoiceBindingSource;

        public MainForm()
        {
            InitializeComponent();
            invoiceSortableBindingList = new SortableBindingList<Invoice>();
            invoiceBindingSource = new BindingSource();
            invoiceBindingSource.DataSource = invoiceSortableBindingList;
            dataStorage = new TempMemoryStorage();
            lDataStorageName.Text = "Временное хранилище";
            InitGrid();
            if (dataStorage != null)
            {
                UpdateBindingSource();
            }
        }

        private void InitGrid()
        {
            InvoiceGrid.AutoGenerateColumns = false;
            InvoiceGrid.AutoSize = true;
            InvoiceGrid.DataSource = invoiceBindingSource;
            InvoiceBindingNavigator.BindingSource = invoiceBindingSource;

            DataGridViewColumn idColumn = new DataGridViewTextBoxColumn();
            idColumn.DataPropertyName = "invoiceId";
            idColumn.Name = "Номер";
            idColumn.SortMode = DataGridViewColumnSortMode.Automatic;

            DataGridViewColumn titleColumn = new DataGridViewTextBoxColumn();
            titleColumn.DataPropertyName = "operationTitle";
            titleColumn.Name = "Название операции";
            titleColumn.SortMode = DataGridViewColumnSortMode.Automatic;

            DataGridViewColumn dateColumn = new DataGridViewTextBoxColumn();
            dateColumn.DataPropertyName = "operationDateTime";
            dateColumn.Name = "Дата";
            dateColumn.SortMode = DataGridViewColumnSortMode.Automatic;


            DataGridViewColumn billColumn = new DataGridViewTextBoxColumn();
            billColumn.DataPropertyName = "Bill";
            billColumn.Name = "Сумма";
            billColumn.SortMode = DataGridViewColumnSortMode.Automatic;

            DataGridViewColumn dateChangeColumn = new DataGridViewTextBoxColumn();
            dateChangeColumn.DataPropertyName = "dateModifyed";
            dateChangeColumn.Name = "Изменено";
            dateChangeColumn.SortMode = DataGridViewColumnSortMode.Automatic;
            dateChangeColumn.ReadOnly = true;

            InvoiceGrid.Columns.Add(idColumn);
            InvoiceGrid.Columns.Add(titleColumn);
            InvoiceGrid.Columns.Add(dateColumn);
            InvoiceGrid.Columns.Add(billColumn);
            InvoiceGrid.Columns.Add(dateChangeColumn);
            
            
        }

        private void UpdateBindingSource()
        {
            invoiceSortableBindingList.Clear();
            List<Invoice> invoices = dataStorage.GetStorageData();
            for (int i = 0; i < invoices.Count; i++)
            {
                invoiceSortableBindingList.Add(invoices[i]);
            }

        }

        private void InvoiceGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Contains(";"))
            {
                string cellValue = InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = cellValue.Replace(";", " ");
                MessageBox.Show("Невозможно использовать символ ';'. Все символы были заменены на пробел.");
                return;
            }
         
            int rowIndex = e.RowIndex;
            Invoice invoice = dataStorage.GetStorageData()[rowIndex];
            switch (e.ColumnIndex)
            {
                case 0:
                    invoice.InvoiceId = (string)InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                    break;
                case 1:
                    invoice.OperationTitle = (string)InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                    break;
                case 2:
                    try
                    {
                        DateTime dateTime = DateTime.Parse(InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                        InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = dateTime.ToLongDateString() + " " + dateTime.ToShortTimeString();
                        invoice.OperationDateTime = dateTime.ToLongDateString() + " " + dateTime.ToShortTimeString();
                    }
                    catch (FormatException)
                    {
                        InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Неверное значение\nПример: 08.12.2015 14:58";
                        invoice.OperationDateTime = null;
                    }
                    
                    break;
                case 3:
                    try
                    {

                        invoice.Bill = Decimal.Parse(InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(), System.Globalization.NumberStyles.Float);
                        
                        //long.Parse(InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    }
                    catch (FormatException )
                    {
                        InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Incorrect value";
                        invoice.Bill = 0;
                    }
                    break;
                
            }
            dataStorage.UpdateRecord(invoice, e.RowIndex);
        }

        private void bindingNavigatorAddNewItem_Click_1(object sender, EventArgs e)
        {
            Invoice invoice = new Invoice();
            dataStorage.AddRecord(invoice);
        }

        public void setActiveDataStorage(String name, AbstractDataStorage<Invoice> data)
        {
            dataStorage = data;
            lDataStorageName.Text = name;
            InvoiceGrid.Rows.Clear();
            UpdateBindingSource();
        }

        private void InvoiceGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            String value = InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            InvoiceGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = value.Trim();
            MessageBox.Show("Неверные данные!", "Ошибка");
        }

        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            string searchingText = tbFilter.Text;
            List<Invoice> filteredData;
            filteredData = dataStorage.GetStorageData().Where(x => (x.InvoiceId.Contains(searchingText))
                                                       || (x.OperationDateTime.Contains(searchingText))
                                                       || (x.OperationTitle.Contains(searchingText)) || (x.Bill.ToString().Contains(searchingText)))
                                                       .ToList();
            setDataToGrid(filteredData);
        }

        private void tbFilterId_TextChanged(object sender, EventArgs e)
        {
            List<Invoice> filteredData;
            filteredData = dataStorage.GetStorageData().Where(x => (x.InvoiceId.Contains(tbFilterId.Text))).ToList();
            setDataToGrid(filteredData);
        }

        private void tbFilterBill_TextChanged(object sender, EventArgs e)
        {
            List<Invoice> filteredData;
            filteredData = dataStorage.GetStorageData().Where(x => (x.Bill.ToString().Contains(tbFilterBill.Text))).ToList();
                setDataToGrid(filteredData);
        }

        private void tbFilterTitle_TextChanged(object sender, EventArgs e)
        {
            List<Invoice> filteredData;
            filteredData = dataStorage.GetStorageData().Where(x => (x.OperationTitle.Contains(tbFilterTitle.Text))).ToList();
            setDataToGrid(filteredData);
        }

        private void setDataToGrid(List<Invoice> data)
        {
            invoiceSortableBindingList.Clear();
            foreach (Invoice invoice in data)
            {
                invoiceSortableBindingList.Add(invoice);
            }
        }

        private void dpFilterDate_ValueChanged(object sender, EventArgs e)
        {
            if (dpFilterDate.Checked)
            {
                List<Invoice> filteredData;
                DateTime dateTime = dpFilterDate.Value;
                filteredData =
                    dataStorage.GetStorageData()
                        .Where(x => (x.OperationDateTime.Contains(dateTime.ToLongDateString())))
                        .ToList();
                setDataToGrid(filteredData);
            }
            else
            {
                setDataToGrid(dataStorage.GetStorageData()); 
            }
        }

        private void craeteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateNewStorageForm createNewStorageForm = new CreateNewStorageForm();
            createNewStorageForm.Show();
        }

        private void connectToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StorageSelectionForm storageSelectionForm = new StorageSelectionForm(this);
            storageSelectionForm.Show();
        }

        private void tempStorageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setActiveDataStorage("Временное хранилище", new TempMemoryStorage());
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            invoiceBindingSource.EndEdit();
            dataStorage.RemoveRecord(invoiceBindingSource.Position);
            invoiceBindingSource.RemoveCurrent();
        }

        private void InvoiceGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            bindingNavigatorDeleteItem.Enabled = InvoiceGrid.RowCount > 0;
        }

        private void InvoiceGrid_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            bindingNavigatorDeleteItem.Enabled = InvoiceGrid.RowCount > 0;
        }
    }
}
