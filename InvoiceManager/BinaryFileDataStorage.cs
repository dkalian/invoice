﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InvoiceManager
{
    class BinaryFileDataStorage : AbstractDataStorage<Invoice>
    {

        private String storageName;
        private List<Invoice> storageData; 

        public BinaryFileDataStorage(String storageName)
        {
            this.storageName = storageName;
            storageData = new List<Invoice>();
        }

        public override void AddRecord(Invoice invoice)
        {
            storageData.Add(invoice);
            UpdateFile();
        }

        public override void UpdateRecord(Invoice invoice, int pos)
        {
            storageData[pos] = invoice;
            UpdateFile();
        }

        public override void RemoveRecord(int pos)
        {
            storageData.RemoveAt(pos);
            UpdateFile();
        }

        public override List<Invoice> GetStorageData()
        {
            Stream stream = null;
            try
            {
                IFormatter formatter = new BinaryFormatter();
                stream = new FileStream(storageName, FileMode.Open, FileAccess.Read, FileShare.Read);
                if (stream.Length != 0)
                storageData = (List<Invoice>)formatter.Deserialize(stream);
            }
            catch (SerializationException exception)
            {
                throw new Exception("Не могу открыть файл!");
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            
            return storageData;
        }

        public void UpdateFile()
        {
            Stream stream = null;
            try{
            IFormatter formatter = new BinaryFormatter();
             stream = new FileStream(storageName, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, storageData);

            }
            catch (IOException)
            {
                MessageBox.Show("Can't write file.", "Write Error");
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }
    }
}
