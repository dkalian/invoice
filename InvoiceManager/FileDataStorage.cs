﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace InvoiceManager
{
    public class FileDataStorage : AbstractDataStorage<Invoice>
    {
        private string storageName;

        public FileDataStorage(string storageName)
        {
            this.storageName = storageName;
        }

        public override void AddRecord(Invoice invoice)
        {
            StreamWriter dataStorage = null;
            try
            {
                dataStorage = new StreamWriter(storageName, true, System.Text.Encoding.GetEncoding(1251));
                dataStorage.WriteLine(invoice.InvoiceId + ";" + invoice.OperationTitle + ";" + invoice.OperationDateTime
                    + ";" + invoice.Bill + ";" + invoice.DateModifyed);
            }
            catch (IOException e)
            {
                Console.Write(e.StackTrace);
            }
            finally
            {
                if (dataStorage != null)
                    dataStorage.Close();
            }

        }

        public override void UpdateRecord(Invoice invoice, int pos)
        {
            List<Invoice> invoiceList = GetStorageData();
            invoiceList[pos] = invoice;
            WriteDataToFile(invoiceList);
        }

        public override void RemoveRecord(int pos)
        {
            List<Invoice> invoiceList = GetStorageData();
            invoiceList.RemoveAt(pos);
            WriteDataToFile(invoiceList);
        }

        private void WriteDataToFile(List<Invoice> invoices)
        {
            StreamWriter dataStorage = null;
            try
            {
                dataStorage = new StreamWriter(storageName, false, System.Text.Encoding.GetEncoding(1251));
                foreach (Invoice invoice1 in invoices)
                    dataStorage.WriteLine(invoice1.InvoiceId + ";" + invoice1.OperationTitle + ";" + invoice1.OperationDateTime
                        + ";" + invoice1.Bill + ";" + invoice1.DateModifyed);
            }
            catch (IOException e)
            {
                throw new Exception("Не могу записать в файл!");
            }
            finally
            {
                if (dataStorage != null)
                    dataStorage.Close();
            }
        }

        public override List<Invoice> GetStorageData()
        {
            StreamReader reader = null;
            List<Invoice> invoiceList = new List<Invoice>();
            try
            {
                reader = new StreamReader(storageName, System.Text.Encoding.GetEncoding(1251));
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (!string.IsNullOrEmpty(line)) {
                        string[] data = line.Split(';');
                        if (data.Length == 5)
                        {
                            invoiceList.Add(new Invoice(data[0].Trim(), data[1].Trim(), data[2].Trim(), decimal.Parse(data[3].Trim()), data[4].Trim()));
                        }
                        else
                        {
                            throw new Exception("Данные повреждены");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Файл поврежден");
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return invoiceList;
        }
    }
}
