﻿using System;
using System.Collections.Generic;

public class TempMemoryStorage : AbstractDataStorage<Invoice>
{
    private List<Invoice> invoiceList;

    public TempMemoryStorage()
    {
        invoiceList = new List<Invoice>();
    }

    public override void AddRecord(Invoice invoice)
    {
        invoiceList.Add(invoice);
    }

    public override void UpdateRecord(Invoice invoice, int pos)
    {
        invoiceList[pos] = invoice;
    }
    
    public override void RemoveRecord(int pos)
    {
        invoiceList.RemoveAt(pos);
    }

    public override List<Invoice> GetStorageData()
    {
        return invoiceList;
    }
}
