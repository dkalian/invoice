﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InvoiceManager
{
    public partial class StorageSelectionForm : Form
    {
        private MainForm mainForm;

        public StorageSelectionForm(MainForm mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
        }

        private void StorageSelectionForm_Load(object sender, EventArgs e)
        {
            try
            {
                string[] files = Directory.GetFiles(Constant.STORAGE_PATH);
                foreach (string file in files)
                {
                    listBox1.Items.Add(file);
                }
            }
            catch (DirectoryNotFoundException directoryNotFound)
            {
                MessageBox.Show("Сперва создайте новое хранилище данных");
                Close();
            }
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            try
            {
                if (listBox1.SelectedItem.ToString().ToLower().EndsWith(".csv"))
                {
                    FileDataStorage fileDataStorage = new FileDataStorage(listBox1.SelectedItem.ToString());
                    mainForm.setActiveDataStorage("CSV Файл: " + listBox1.SelectedItem, fileDataStorage);
                }
                if (listBox1.SelectedItem.ToString().ToLower().EndsWith(".bin"))
                {
                    BinaryFileDataStorage binaryFileDataStorage = new BinaryFileDataStorage(listBox1.SelectedItem.ToString());
                    mainForm.setActiveDataStorage("Бинарный файл: " + listBox1.SelectedItem, binaryFileDataStorage);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                mainForm.setActiveDataStorage("Временное хранилище", new TempMemoryStorage());
            }
        }
    }
}
