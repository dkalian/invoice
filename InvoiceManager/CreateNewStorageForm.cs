﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InvoiceManager
{
    public partial class CreateNewStorageForm : Form
    {
        public CreateNewStorageForm()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (IsFieldsValidated())
            {
                if (!Directory.Exists(Constant.STORAGE_PATH + tbStorageName.Text.ToString()))
                    Directory.CreateDirectory(Constant.STORAGE_PATH);  // craete directory
                String fileName = Constant.STORAGE_PATH + tbStorageName.Text; // file name
                switch (cbStorageType.SelectedIndex)
                {
                    case 0:
                        fileName += ".csv";
                        break;
                    case 1:
                        fileName += ".bin";
                        break;
                }
                FileStream fileStream = File.Create(fileName);
                fileStream.Close();
                Close();
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Ошибка", MessageBoxButtons.OK);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool IsFieldsValidated()
        {
            return !(cbStorageType.SelectedIndex == -1 || tbStorageName.Text.Length == 0);
        }

        private void CreateNewStorageForm_Load(object sender, EventArgs e)
        {
            cbStorageType.SelectedIndex = 0;
        }
    }
}
