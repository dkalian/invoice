﻿using System;
using System.Collections.Generic;

public abstract class AbstractDataStorage<T> : IDataStorage<T>
{
    public abstract void AddRecord(T t);

    public abstract void UpdateRecord(T t, int pos);

    public abstract void RemoveRecord(int pos);

    public abstract List<T> GetStorageData();
}

public interface IDataStorage<T>
{
    /// <summary>
    /// Adds a new  object in storage
    /// </summary>
    /// <param name="t">object to add in storage</param>
    void AddRecord(T t);

    /// <summary>
    /// Updates an  object in storage
    /// </summary>
    /// <param name="t"> object to update in storage</param>
    void UpdateRecord(T t, int pos);

    /// <summary>
    /// Removes an  object from storage
    /// </summary>
    /// <param name="t"> object to remove from storage</param>
    void RemoveRecord(int pos);

    List<T> GetStorageData();
}
