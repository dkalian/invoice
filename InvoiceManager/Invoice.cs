﻿using System;
using System.Runtime.Serialization;

[Serializable]
public class Invoice
{
    public Invoice(string invoiceId, string operationTitle, string operationDateTime, decimal bill, string dateModifyed)
    {
        this.invoiceId = invoiceId;
        this.operationTitle = operationTitle;
        this.operationDateTime = operationDateTime;
        this.bill = bill;
        this.dateModifyed = dateModifyed;
    }

    public Invoice()
    {
        this.invoiceId = "";
        this.operationTitle = "";
        this.operationDateTime = "";
        this.DateModifyed = "";
        this.bill = 0;
    }

    private string invoiceId;
    private string operationTitle; // primary field
    private string operationDateTime; // operation datetime
    /// <summary>
    /// </summary>
    /// <remarks></remarks>
    private decimal bill;
    /// <remarks></remarks>
    private string dateModifyed;

    public string InvoiceId
    {
        get { return invoiceId; }
        set
        {
            invoiceId = value;
            dateModifyed = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        }
    }

    public string OperationTitle
    {
        get { return operationTitle; }
        set
        {
            operationTitle = value;
            dateModifyed = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        }
    }

    public string OperationDateTime
    {
        get { return operationDateTime; }
        set
        {
            operationDateTime = value;
            dateModifyed = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        }
    }

    public decimal Bill
    {
        get { return bill; }
        set
        {
            bill = value;
            dateModifyed = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        }  
    }

    public string DateModifyed
    {
        get { return dateModifyed; }
        set { dateModifyed = value; }
    }

}